﻿using CarkparkStationMonitor.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CarkparkStationMonitor.Controllers
{
    public class DashboardController : Controller
    {
        //
        // GET: /Dashboard/
        private DBConnection db = new DBConnection();
        private int total;

        public ActionResult Index()
        {
            setContent();
            setSList();
            return View();
        }

        private void setContent()
        {
            String all = "_";
            String is_new = "1";
            ViewBag.pmsStat = count(db.getInactivePMS(all));
            ViewBag.pmsStatNw = count(db.getInactivePMS(is_new));

            ViewBag.stationStat = count(db.getStationStatus(all));
            ViewBag.stationStatNw = count(db.getStationStatus(is_new));
           
            ViewBag.stationAlarmBAD = count(db.getStationAlarm(0,all)); 
            ViewBag.stationAlarmBADNw = count(db.getStationAlarm(0, is_new)); 

            ViewBag.stationAlarmPOP = count(db.getStationAlarm(5, all));//1
            ViewBag.stationAlarmPOPNw = count(db.getStationAlarm(5, is_new));//1

            ViewBag.stationAlarmCPF = count(db.getStationAlarm(2, all));
            ViewBag.stationAlarmCPFNw = count(db.getStationAlarm(2, is_new));

            ViewBag.stationAlarmDF = count(db.getStationAlarm(3, all));
            ViewBag.stationAlarmDFNw = count(db.getStationAlarm(3, is_new));

            ViewBag.diskSpace = count(db.getStationDiskSpace(all));
            ViewBag.diskSpaceNw = count(db.getStationDiskSpace(is_new));

            ViewBag.noInfo = count(db.getStationNoEntryInfo(all));
            ViewBag.noInfoNw = count(db.getStationNoEntryInfo(is_new));

            ViewBag.sync = count(db.getStationLatency(all));
            ViewBag.syncNw = count(db.getStationLatency(is_new));

            ViewBag.genTime = db.getGeneratedTime();
            total = ViewBag.pmsStat + ViewBag.stationStat + ViewBag.stationAlarmBAD + ViewBag.stationAlarmCPF + ViewBag.stationAlarmPOP + ViewBag.stationAlarmDF + ViewBag.diskSpace + ViewBag.noInfo + ViewBag.sync;
        }

        private void setSList()
        {
            if (Session["sList"] != null)
            {
                String temp = Session["sList"] + "," + total;
                Session["sList"] = temp;
            }
            else
            {
                Session["sList"] = total.ToString();
            }

            if (Session["sMax"] != null)
            {
                if ((Int32)Session["sMax"] > total)
                    Session["sMax"] = total + 50;
            }
            else
            {
                Session["sMax"] = total + 50;
            }




        }
        private int count(SqlDataReader dr)
        {
            int value = 0;
            if (dr.HasRows)
                value =  dr.Cast<object>().Count();
            db.closeConn();
            return value;
           
        }

        public String setBg(int ival)
        {
            if (ival > 10)
                return "red";
            else if (ival > 0)
                return "yellow";
            return "green";

        }
        public string getSList()
        {
            return (string)Session["sList"];
        }

        public int getMax()
        {
            return (Int32)Session["sMax"];
        }
    }
}
