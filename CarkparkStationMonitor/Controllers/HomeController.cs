﻿using CarkparkStationMonitor.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Web;
using System.Web.Mvc;

namespace CarkparkStationMonitor.Controllers
{
    public class HomeController : Controller
    {
      
        //
        // GET: /Home/
        private DBConnection db = new DBConnection();
        private SqlDataReader dr;
        private String currenUserName = Environment.UserName;

        public ActionResult Index()
        {
            ViewBag.genTime = db.getGeneratedTime();
            return View();
        }

        [HttpPost]
        public ActionResult attended(Tasks task)
        {
           //JRX-ddMMyyyyhhmmss-ffff
            String pk = "JRX-"+DateTime.Now.ToString("ddMMyyyyhhmmss-fff");

            if (task.selected)
                task.status = "IP";
            else
                task.status = "S";
            task.assignedby = currenUserName;
            task.updatedby = currenUserName;
            task.pk = pk;
            db.task(task);
            return Json(new { success = true });

        }

        [HttpPost]
        public HtmlString isNewSpan(String a, String b)
        {
            if (String.Compare(a, b) == 0)
                return new HtmlString("<span class='label label-success'>New</span>");
            return new HtmlString("");
        }
        [HttpPost]
        public HtmlString isChecked(Object status)
        {
            if(status == DBNull.Value)
                return new HtmlString("");
            else if (String.Compare((String) status, "S") == 0)
                return new HtmlString("");
            else if (String.Compare((String)status, "IP") == 0)
                return new HtmlString("checked");

            return new HtmlString("");
            
        }
        [HttpPost]
        public HtmlString sort(Object status)
        {
            if (status == DBNull.Value)
                return new HtmlString("0");
            else if (String.Compare((String)status, "S") == 0)
                return new HtmlString("0");
            else if (String.Compare((String)status, "IP") == 0)
                return new HtmlString("1");

            return new HtmlString("0");

        }
        [HttpPost]
        public SqlDataReader getStationStat(string is_new)
        {
            return db.getStationStatus(is_new);
        }
         [HttpPost]
        public SqlDataReader getSpaceStat(string is_new)
        {
            return db.getStationDiskSpace(is_new);
        }
         [HttpPost]
        public SqlDataReader getNoEntryInfo(string is_new)
        {
            return db.getStationNoEntryInfo(is_new);
        }
         [HttpPost]
        public SqlDataReader getStationAlarm(int type, string is_new)
        {
            return db.getStationAlarm(type,is_new);
        }
         [HttpPost]
        public SqlDataReader getStationLatency(string is_new)
        {
            return db.getStationLatency(is_new);
        }
         [HttpPost]
        public SqlDataReader getInactivePMS(string is_new)
        {
            return db.getInactivePMS(is_new);
        }
         [HttpPost]
        public bool LocalPing(String Ip)
        {

            Ping pingSender = new Ping();
            PingReply reply = pingSender.Send(Ip);

            if (reply.Status == IPStatus.Success)
                return true;
            return false;
        }
         [HttpPost]
        public void closeConn()
        {
            db.closeConn();
        }

    }
}
