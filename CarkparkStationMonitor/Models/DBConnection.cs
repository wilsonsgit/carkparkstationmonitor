﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CarkparkStationMonitor.Models
{
    public class DBConnection
    {
        private static String sConString = ConfigurationManager.ConnectionStrings["CarparkCentralConnectionString"].ToString();
        private SqlConnection sqlConnection =  new SqlConnection(sConString);
        SqlCommand sm;


        public SqlDataReader getStationStatus(string is_new)
        {
            String linq = 
                        " Declare  @ARCHIVEID VARCHAR(60), @ALARMTYPE VARCHAR(2), @ISNEW VARCHAR(1)                                                                             " +
                        "                                                                                                                                                       " +
                        " SET @ISNEW = '" + is_new + "'                                                                                                                                      " +
                        " SET @ARCHIVEID = (                                                                                                                                    " +
                        "       SELECT TOP 1 [ARCHIVEID]                                                                                                                        " +
                        "       FROM [CarParkCentral].[dbo].[CP_ARCHIVE]                                                                                                        " +
                        "       WHERE CAST([DATETIME] AS DATE) = CAST(GETDATE() AS DATE)                                                                                        " +
                        "       ORDER BY [DATETIME] DESC                                                                                                                        " +
                        " )                                                                                                                                                     " +
                        "                                                                                                                                                       " +
                        " ;with [ARCHIVE_TABLE] as (                                                                                                                            " +
                        "       SELECT CPCODE, SIPADDRESS FROM [CarParkCentral].[dbo].[CP_STATUS_STATIONS_archive]                                                              " +
                        "       WHERE ARCHIVEID = (@ARCHIVEID)                                                                                                                  " +
                        "       AND (STATUS = 0)                                                                                                                                  " +
                        "                                                                                                                                                       " +
                        " ),[MAIN_TABLE] as(                                                                                                                                    " +
                        " 	SELECT A.CPCODE,B.CPIPADDRESS, STATION_ID, SIPADDRESS, STATUS, CASE WHEN DEVICE_TIME <> '' THEN CONVERT(varchar, CAST(DEVICE_TIME AS DATETIME), 100) ELSE 'NO TIME' END AS  CLOCK, A.TASK_ID, A.ID       " +
                        " 	FROM [CarParkCentral].[dbo].[CP_STATUS_STATIONS] A                                                                                                  " +
                        " 	RIGHT JOIN [CarParkCentral].[dbo].[CP_STATUS] B ON A.CPCODE = B.CPCODE                                                                              " +
                        " 	WHERE (A.STATUS = 0)                                                                                                                                    " +
                        " ),[RESULT] as(                                                                                                                                        " +
                        " 	SELECT NEW.*,                                                                                                                                       " +
                        " 	CASE WHEN ARCH.CPCODE IS NULL THEN '1' ELSE '0' END AS 'IS_NEW'                                                                                     " +
                        " 	FROM [MAIN_TABLE] new                                                                                                                               " +
                        " 	LEFT JOIN [ARCHIVE_TABLE] arch                                                                                                                      " +
                        " 	ON new.CPCODE = arch.CPCODE AND new.SIPADDRESS = arch.SIPADDRESS                                                                                    " +
                        " ),[FINAL_2] as (                                                                                                 " +
                        " 	SELECT S.*, C.LINKSERVER_NAME FROM (                                                                           " +
                        " 		SELECT A.*, B.CPIPADDRESS AS CPIP                                                                          " +
                        " 		FROM [RESULT] A                                                                                             " +
                        " 		LEFT JOIN [CarParkCentral].[dbo].[CP_STATUS] B ON B.CPCODE = A.CPCODE                                      " +
                        " 	)S                                                                                                             " +
                        " 	LEFT JOIN (                                                                                                    " +
                        " 		SELECT Distinct(CPIP), LINKSERVER_NAME                                                                     " +
                        " 		FROM [CarParkCentral].[dbo].[CarParkVPNList]                                                               " +
                        " 	) C ON C.CPIP = S.CPIP                                                                                         " +
                        " )                                                                                                                " +
                        "                                                                                                                                                       " +
                        " SELECT FINAL.*, TASK.TASK_STATUS FROM [FINAL_2] FINAL                                                                                                    " +
                        " LEFT JOIN [CarParkCentral].[dbo].[CP_NOTIFICATION_TASK] TASK ON FINAL.TASK_ID = TASK.TASK_ID                                                          "+
                        " WHERE IS_NEW LIKE @ISNEW                                                                                                                              " +
                        " ORDER BY IS_NEW DESC, CPCODE, STATION_ID ASC                                                                                                          ";


            sm = new SqlCommand();
            sm.Connection = sqlConnection;
            sm.CommandText = linq;
            sm.CommandType = CommandType.Text;
            openConn();
            SqlDataReader dr = sm.ExecuteReader();
            return dr;
           
        }

        public SqlDataReader getStationDiskSpace(string is_new)
        {
            String linq = " Declare  @ARCHIVEID VARCHAR(60), @ALARMTYPE VARCHAR(2), @ISNEW VARCHAR(1)                                            " +
                        "                                                                                                                      " +
                        " SET @ISNEW = '" + is_new + "'                                                                                                     " +
                        " SET @ARCHIVEID = (                                                                                                   " +
                        "       SELECT TOP 1 [ARCHIVEID]                                                                                       " +
                        "       FROM [CarParkCentral].[dbo].[CP_ARCHIVE]                                                                       " +
                        "       WHERE CAST([DATETIME] AS DATE) = CAST(GETDATE() AS DATE)                                                       " +
                        "       ORDER BY [DATETIME] DESC                                                                                       " +
                        " )                                                                                                                    " +
                        "                                                                                                                      " +
                        " ;with [ARCHIVE_TABLE] as (                                                                                           " +
                        "       SELECT * FROM [CarParkCentral].[dbo].[CP_STATUS_DSPACE_archive]                                                " +
                        "       WHERE ARCHIVEID = (@ARCHIVEID)                                                                                 " +
                        "       AND CAST(SPACE AS FLOAT) <= 5000.00                                                                            " +
                        "                                                                                                                      " +
                        " ),[MAIN_TABLE] as(                                                                                                   " +
                        " 	SELECT A.CPCODE,B.CPIPADDRESS, CONVERT(varchar, CAST(CLOCK AS DATETIME), 100)AS CLOCK, DRIVE, SPACE, A.TASK_ID, A.ID	   " +
                        " 	FROM [CarParkCentral].[dbo].[CP_STATUS_DSPACE] A                                                                   " +
                        " 	RIGHT JOIN [CarParkCentral].[dbo].[CP_STATUS] B ON A.CPCODE = B.CPCODE                                             " +
                        " 	AND CAST(SPACE AS FLOAT) <= 5000.00                                                                                " +
                        " ),[RESULT] as(                                                                                                       " +
                        " 	SELECT NEW.*,                                                                                                      " +
                        " 	CASE WHEN ARCH.CPCODE IS NULL THEN '1' ELSE '0' END AS 'IS_NEW'                                                    " +
                        " 	FROM [MAIN_TABLE] new                                                                                              " +
                        " 	LEFT JOIN [ARCHIVE_TABLE] arch                                                                                     " +
                        " 	ON new.CPCODE = arch.CPCODE AND new.DRIVE = arch.DRIVE                                                             " +
                        " 	AND new.TASK_ID = arch.TASK_ID                                                                                     " +
                        " ),[FINAL] as (                                                                                                 " +
                        " 	SELECT S.*, C.LINKSERVER_NAME FROM (                                                                           " +
                        " 		SELECT A.*, B.CPIPADDRESS AS CPIP                                                                          " +
                        " 		FROM [RESULT] A                                                                                             " +
                        " 		LEFT JOIN [CarParkCentral].[dbo].[CP_STATUS] B ON B.CPCODE = A.CPCODE                                      " +
                        " 	)S                                                                                                             " +
                        " 	LEFT JOIN (                                                                                                    " +
                        " 		SELECT Distinct(CPIP), LINKSERVER_NAME                                                                     " +
                        " 		FROM [CarParkCentral].[dbo].[CarParkVPNList]                                                               " +
                        " 	) C ON C.CPIP = S.CPIP                                                                                         " +
                        " )                                                                                                                " +
                        "                                                                                                                      " +
                        " SELECT FINAL.*, TASK.TASK_STATUS FROM FINAL FINAL                                                                   " +
                        " LEFT JOIN [CarParkCentral].[dbo].[CP_NOTIFICATION_TASK] TASK ON FINAL.TASK_ID = TASK.TASK_ID                         " +
                        " WHERE [IS_NEW] LIKE @ISNEW                                                                                           " +
                        " AND CPCODE IS NOT NULL                                                                                               " +
                        " AND DRIVE IS NOT NULL                                                                                                " +
                        " AND [SPACE] IS NOT NULL                                                                                              " +
                        " ORDER BY IS_NEW DESC, CPCODE, CPIPADDRESS																			   ";

            sm = new SqlCommand();
            sm.Connection = sqlConnection;
            sm.CommandText = linq;
            sm.CommandType = CommandType.Text;
            openConn();
            SqlDataReader dr = sm.ExecuteReader();
            return dr;

        }

        public SqlDataReader getStationNoEntryInfo(string is_new)
        {

            String linq = " DECLARE @ARCHIVEID VARCHAR(60), @ALARMTYPE VARCHAR(2), @ISNEW VARCHAR(1)                        " +
                    "                                                                                                 " +
                    " SET @ISNEW = '" + is_new + "'                                                                                " +
                    " SET @ARCHIVEID = (                                                                              " +
                    "       SELECT TOP 1 [ARCHIVEID]                                                                  " +
                    "       FROM [CarParkCentral].[dbo].[CP_ARCHIVE]                                                  " +
                    "       WHERE CAST([DATETIME] AS DATE) = CAST(GETDATE() AS DATE)                                  " +
                    "       ORDER BY [DATETIME] DESC                                                                  " +
                    " )                                                                                               " +
                    " ;with [ARCHIVE_TABLE] as (	                                                                  " +
                    " 	SELECT DISTINCT CPIPADDRESS, CAST(NO_ENTRY_INFO AS INTEGER) AS NO_ENTRY_INFO, TASK_ID         " +
                    " 	FROM [CarParkCentral].[dbo].[CP_STATUS_archive]                                               " +
                    " 	WHERE ARCHIVEID = (@ARCHIVEID)                                                                " +
                    " 	AND NO_ENTRY_INFO > 20                                                                        " +
                    "                                                                                                 " +
                    " ),[MAIN_TABLE] as(                                                                              " +
                    " 	SELECT                                                                                        " +
                    " 		DISTINCT A.CPIPADDRESS,                                                                   " +
                    " 		CAST(A.NO_ENTRY_INFO AS INTEGER) AS NO_ENTRY_INFO,                                        " +
                    " 		CONVERT(varchar, CAST(A.CLOCK AS DATETIME),100) AS CLOCK,                                 " +
                    " 		A.TASK_ID, A.ID,                                                                          " +
                    " 		STUFF(( 						                                                          " +
                    " 				SELECT ',' + B.CPCODE                                                             " +
                    " 				From [CarParkCentral].[dbo].[CP_STATUS] B                                         " +
                    " 				WHERE A.CPIPADDRESS = B.CPIPADDRESS                                               " +
                    " 				FOR XML PATH('')                                                                  " +
                    " 			),1, 1, ''                                                                            " +
                    " 		) AS CPCODE                                                                               " +
                    " 	FROM [CarParkCentral].[dbo].[CP_STATUS] A					                                  " +
                    " 	WHERE A.NO_ENTRY_INFO > 20    			                                              	      " +
                    "                                                                                                 " +
                    " ),[RESULT] as(                                                                                  " +
                    " 	SELECT NEW.*,                                                                                 " +
                    " 	CASE                                                                                          " +
                    " 		WHEN CAST(NEW.NO_ENTRY_INFO AS INTEGER) <> CAST(ARCH.NO_ENTRY_INFO AS INTEGER)            " +
                    " 	THEN '0' ELSE '1' END AS 'IS_NEW'                                                             " +
                    " 	FROM [MAIN_TABLE] new                                                                         " +
                    " 	LEFT JOIN [ARCHIVE_TABLE] arch                                                                " +
                    " 	ON new.CPIPADDRESS = arch.CPIPADDRESS                                                         " +
                    " 	AND new.NO_ENTRY_INFO = arch.NO_ENTRY_INFO                                                    " +
                    " 	AND new.TASK_ID = arch.TASK_ID                                                                " +
                    " ),[FINAL] as (                                                                                                 " +
                    " 	SELECT S.*, C.LINKSERVER_NAME FROM (                                                                           " +
                    " 		SELECT A.*, B.CPIPADDRESS AS CPIP                                                                          " +
                    " 		FROM [RESULT] A                                                                                             " +
                    " 		LEFT JOIN [CarParkCentral].[dbo].[CP_STATUS] B ON B.CPCODE = A.CPCODE                                      " +
                    " 	)S                                                                                                             " +
                    " 	LEFT JOIN (                                                                                                    " +
                    " 		SELECT Distinct(CPIP), LINKSERVER_NAME                                                                     " +
                    " 		FROM [CarParkCentral].[dbo].[CarParkVPNList]                                                               " +
                    " 	) C ON C.CPIP = S.CPIPADDRESS                                                                                         " +
                    " )                                                                                                                " +
                    " SELECT * FROM                                                                                   " +
                    " (                                                                                               " +
                    " 	SELECT                                                                                        " +
                    " 	FINAL.*,                                                                                      " +
                    " 	TASK.TASK_STATUS ,                                                                            " +
                    " 	ROW_NUMBER() OVER(PARTITION BY CPIPADDRESS ORDER BY ID DESC) RN                               " +
                    " 	FROM FINAL                                                                                      " +
                    " 	LEFT JOIN [CarParkCentral].[dbo].[CP_NOTIFICATION_TASK] TASK ON FINAL.TASK_ID = TASK.TASK_ID  " +
                    " 	WHERE [IS_NEW] LIKE @ISNEW                                                                    " +
                    " 	                                                                                              " +
                    " )S                                                                                              " +
                    " WHERE S.RN = 1                                                                                  " +
                    " ORDER BY IS_NEW DESC, NO_ENTRY_INFO DESC, CPCODE                                                 ";
                                                                                                    

            sm = new SqlCommand();
            sm.Connection = sqlConnection;
            sm.CommandText = linq;
            sm.CommandType = CommandType.Text;
            openConn();
            SqlDataReader dr = sm.ExecuteReader();
            return dr;

        }
        public SqlDataReader getStationAlarm(int type, string is_new)
        {

            String linq =
                " Declare  @ARCHIVEID VARCHAR(60), @ALARMTYPE VARCHAR(2), @ISNEW VARCHAR(1)                                        " +
                "                                                                                                                  " +
                " SET @ISNEW = '" + is_new + "'                                                                                    " +
                " SET @ALARMTYPE = " + type + "                                                                                    " +
                " SET @ARCHIVEID = (                                                                                               " +
                "       SELECT TOP 1 [ARCHIVEID]                                                                                   " +
                "       FROM [CarParkCentral].[dbo].[CP_ARCHIVE]                                                                   " +
                "       WHERE CAST([DATETIME] AS DATE) = CAST(GETDATE() AS DATE)                                                   " +
                "       ORDER BY [DATETIME] DESC                                                                                   " +
                " )                                                                                                                " +
                "                                                                                                                  " +
                " ;with [ARCHIVE_TABLE] as (                                                                                       " +
                "       SELECT * FROM [CarParkCentral].[dbo].CP_STATUS_STATIONS_ALARM_archive                                      " +
                "       WHERE ARCHIVEID = (@ARCHIVEID) AND ALARM_MSG = @ALARMTYPE                                                  " +
                "                                                                                                                  " +
                " ),[MAIN_TABLE] as(                                                                                               " +
                "       SELECT * FROM(                                                                                             " +
                "             SELECT [CPCODE],[STATION_ID],[SIPADDRESS],[DESCRIPTION] AS ALARM_MSG,  A.ALARM_MSG AS MSG_C0DE, A.TASK_ID,A.ID,    " +
                "             CONVERT(varchar, CAST(TIME AS DATETIME), 100)AS TIME, TIME AS TIME2                                  " +
                "             FROM [CarParkCentral].[dbo].[CP_STATUS_STATIONS_ALARM] A                                             " +
                "             LEFT JOIN [CarParkCentral].[dbo].[CP_STATUS_CODE] B ON A.ALARM_MSG = B.CODE                          " +
                "          WHERE A.ALARM_MSG = @ALARMTYPE                                                                          " +
                "       )S                                                                                                         " +
                "       WHERE S.[TIME2] > (SELECT DATEADD(minute, -60, CURRENT_TIMESTAMP))                                         " +
                " ),[RESULT] as(                                                                                                   " +
                " 	SELECT NEW.*,                                                                                                  " +
                " 	CASE WHEN ARCH.CPCODE IS NULL THEN '1' ELSE '0' END AS 'IS_NEW'                                                " +
                " 	FROM [MAIN_TABLE] new                                                                                          " +
                " 	LEFT JOIN [ARCHIVE_TABLE] arch                                                                                 " +
                " 	ON new.CPCODE = arch.CPCODE AND new.SIPADDRESS = arch.SIPADDRESS AND new.MSG_C0DE = arch.ALARM_MSG             " +
                " ),[FINAL] as(                                                                                                    " +
                " SELECT FINAL.*, TASK.TASK_STATUS FROM RESULT FINAL                                                               " +
                " LEFT JOIN [CarParkCentral].[dbo].[CP_NOTIFICATION_TASK] TASK ON FINAL.TASK_ID = TASK.TASK_ID                     " +
                " WHERE IS_NEW LIKE @ISNEW                                                                                         " +
                " ),[FINAL_2] as (                                                                                                 " +
                " 	SELECT S.*, C.LINKSERVER_NAME FROM (                                                                           " +
                " 		SELECT A.*, B.CPIPADDRESS                                                                                  " +
                " 		FROM [FINAL] A                                                                                             " +
                " 		LEFT JOIN [CarParkCentral].[dbo].[CP_STATUS] B ON B.CPCODE = A.CPCODE                                      " +
                " 	)S                                                                                                             " +
                " 	LEFT JOIN (                                                                                                    " +
                " 		SELECT Distinct(CPIP), LINKSERVER_NAME                                                                     " +
                " 		FROM [CarParkCentral].[dbo].[CarParkVPNList]                                                               " +
                " 	) C ON C.CPIP = S.CPIPADDRESS                                                                                  " +
                " )                                                                                                                ";

      

            switch (type){
                case 3:  
                        linq +=
                        "                                                                       "+
                        " SELECT A.*, B.TOTAL from [FINAL_2] A                                    "+
                        " LEFT JOIN (                                                           "+
                        " SELECT * FROM(                                                        "+
                        "     SELECT CPCODE, SIPADDRESS, COUNT(STATION_ID) AS [TOTAL]           "+
                        "     FROM [CarParkCentral].[dbo].[CP_STATUS_STATIONS_ALARM_archive]    "+
                        "     WHERE CONVERT(DATETIME,[TIME]) > DATEADD(MI,-60,GETDATE())       "+
                        "     GROUP BY CPCODE, SIPADDRESS                                       "+
                        " )S                                                                    "+
                        " WHERE TOTAL > 10 ) b                                                  "+
                        " on a.SIPADDRESS = b.SIPADDRESS                                        "+
                        " WHERE b.SIPADDRESS IS NOT NULL                                        ";
                        break;
                default:
                        linq +=
                        "                       " +
                        " SELECT * FROM [FINAL_2] " +
                        " ORDER BY IS_NEW DESC, CPCODE, STATION_ID ASC ";
                        break;
            }
            sm = new SqlCommand();
            sm.Connection = sqlConnection;
            sm.CommandText = linq;
            sm.CommandType = CommandType.Text;
            openConn();
            SqlDataReader dr = sm.ExecuteReader();
            return dr;

        }

  
        public SqlDataReader getStationLatency(string is_new)
        {

            String linq = " DECLARE @CodeNameString varchar(1000),@ARCHIVEID VARCHAR(60), @ALARMTYPE VARCHAR(2), @ISNEW VARCHAR(1)                             " +
                        "                                                                                                                                    " +
                        " SET @ISNEW = '" + is_new + "'                                                                                                                   " +
                        " SET @ARCHIVEID = (                                                                                                                 " +
                        "       SELECT TOP 1 [ARCHIVEID]                                                                                                     " +
                        "       FROM [CarParkCentral].[dbo].[CP_ARCHIVE]                                                                                     " +
                        "       WHERE CAST([DATETIME] AS DATE) = CAST(GETDATE() AS DATE)                                                                     " +
                        "       ORDER BY [DATETIME] DESC                                                                                                     " +
                        " )                                                                                                                                  " +
                        "                                                                                                                                    " +
                        " ;with [ARCHIVE_TABLE] as (                                                                                                         " +
                        " 	SELECT A.CPCODE,B.STATION_ID, A.TASK_ID,ARCHIVEID                                                                                " +
                        " 	FROM [CarParkCentral].[dbo].[CP_STATUS_archive] A                                                                                " +
                        " 	LEFT JOIN [CarParkCentral].[dbo].[CP_STATUS_STATIONS] B ON B.CPCODE = A.CPCODE                                                   " +
                        " 	WHERE STATUS <> -1                                                                                                               " +
                        " 	AND ARCHIVEID = @ARCHIVEID                                                                                                       " +
                        " ),[MAIN_TABLE] as(                                                                                                                 " +
                        " 		SELECT * FROM(                                                                                                               " +
                        " 		SELECT A.CPCODE,B.STATION_ID, CLOCK AS PMS_TIME, B.DEVICE_TIME, datediff(MINUTE,B.DEVICE_TIME,CLOCK) AS LATENCY, A.TASK_ID,A.ID   " +
                        " 		FROM [CarParkCentral].[dbo].[CP_STATUS] A                                                                                    " +
                        " 		LEFT JOIN [CarParkCentral].[dbo].[CP_STATUS_STATIONS] B ON B.CPCODE = A.CPCODE                                               " +
                        " 		WHERE STATUS <> -1                                                                                                           " +
                        " 	)C                                                                                                                               " +
                        " 	WHERE LATENCY >= 2       			                                              	                                             " +
                        " ),[RESULT] as(                                                                                                                     " +
                        " 	SELECT NEW.*,                                                                                                                    " +
                        " 	CASE WHEN ARCH.CPCODE IS NULL THEN '1' ELSE '0' END AS 'IS_NEW'                                                                  " +
                        " 	FROM [MAIN_TABLE] new                                                                                                            " +
                        " 	LEFT JOIN [ARCHIVE_TABLE] arch                                                                                                   " +
                        " 	ON new.CPCODE = arch.CPCODE                                                                                                      " +
                        " 	AND NEW.STATION_ID = arch.STATION_ID                                                                                             " +
                        " ),[FINAL] as (                                                                                                 " +
                        " 	SELECT S.*, C.LINKSERVER_NAME FROM (                                                                           " +
                        " 		SELECT A.*, B.CPIPADDRESS AS CPIP                                                                          " +
                        " 		FROM [RESULT] A                                                                                             " +
                        " 		LEFT JOIN [CarParkCentral].[dbo].[CP_STATUS] B ON B.CPCODE = A.CPCODE                                      " +
                        " 	)S                                                                                                             " +
                        " 	LEFT JOIN (                                                                                                    " +
                        " 		SELECT Distinct(CPIP), LINKSERVER_NAME                                                                     " +
                        " 		FROM [CarParkCentral].[dbo].[CarParkVPNList]                                                               " +
                        " 	) C ON C.CPIP = S.CPIP                                                                                         " +
                        " )                                                                                                                " +

                        " SELECT FINAL.*, TASK.TASK_STATUS FROM FINAL                                                                                 " +
                        " LEFT JOIN [CarParkCentral].[dbo].[CP_NOTIFICATION_TASK] TASK ON FINAL.TASK_ID = TASK.TASK_ID                                       " +
                        " WHERE [IS_NEW] LIKE @ISNEW                                                                                                         " +
                        " ORDER BY IS_NEW DESC, CPCODE, STATION_ID                                                                                           ";




            sm = new SqlCommand();
            sm.Connection = sqlConnection;
            sm.CommandText = linq;
            sm.CommandType = CommandType.Text;
            openConn();
            SqlDataReader dr = sm.ExecuteReader();
            return dr;

        }
        public SqlDataReader getInactivePMS(string is_new)
        {

            String linq = " Declare  @ARCHIVEID VARCHAR(60), @ALARMTYPE VARCHAR(2), @ISNEW VARCHAR(1)    " +
                            " SET @ISNEW = '"+is_new+"'                                                             " +
                            " SET @ALARMTYPE = 3                                                           " +
                            " ;with [ARCHIVE_TABLE] as (                                                   " +
                            "       SELECT * FROM [CarParkCentral].[dbo].[CP_CONN_STAT_E]                  " +
                            " 		WHERE ERROR_CODE = (                                                   " +
                            " 			SELECT TOP 1 ERROR_CODE FROM (                                     " +
                            " 				SELECT TOP 2 *                                                 " +
                            " 				FROM [CarParkCentral].[dbo].[CP_CONN_STAT_V]                   " +
                            " 				WHERE JOB_FROM = 'NetworkChecker.ps1'                          " +
                            " 				ORDER BY DATETIMECREATED DESC                                  " +
                            " 			)S                                                                 " +
                            " 			ORDER BY DATETIMECREATED 	                                       " +
                            " 		)                                                                      " +
                            " ),[MAIN_TABLE] as(                                                           " +
                            " 	SELECT * FROM [CarParkCentral].[dbo].[CP_CONN_STAT_E]                      " +
                            " 	WHERE ERROR_CODE = (                                                       " +
                            " 		SELECT TOP 1 ERROR_CODE                                                " +
                            " 		FROM [CarParkCentral].[dbo].[CP_CONN_STAT_V]                           " +
                            " 		WHERE JOB_FROM = 'NetworkChecker.ps1'                                  " +
                            " 		ORDER BY DATETIMECREATED DESC                                          " +
                            " 	)                                                                          " +
                            " ),[RESULT] as(                                                               " +
                            " 	SELECT NEW.*,                                                              " +
                            " 	CASE WHEN ARCH.CPCODE IS NULL THEN '1' ELSE '0' END AS 'IS_NEW'            " +
                            " 	FROM [MAIN_TABLE] new                                                      " +
                            " 	LEFT JOIN [ARCHIVE_TABLE] arch                                             " +
                            " 	ON new.CPCODE = arch.CPCODE                                                " +
                            "                                                                              " +
                            " )                                                                            " +
                            " SELECT FINAL.*, TASK.TASK_STATUS FROM RESULT FINAL                                                                   " +
                            " LEFT JOIN [CarParkCentral].[dbo].[CP_NOTIFICATION_TASK] TASK ON FINAL.TASK_ID = TASK.TASK_ID                         " +
                            " WHERE IS_NEW LIKE @ISNEW                                                     " +
                            " ORDER BY IS_NEW DESC, CPCODE                                                 ";

            sm = new SqlCommand();
            sm.Connection = sqlConnection;
            sm.CommandText = linq;
            sm.CommandType = CommandType.Text;
            openConn();
            SqlDataReader dr = sm.ExecuteReader();
            return dr;

        }

        public SqlDataReader task(Tasks task)
        {
            String linq =   " Declare                                                                                                                      "+                                                                                                  
                            " @TABLE_NAME VARCHAR(50),@TABLE_CODE VARCHAR(10),@QRY NVARCHAR(max),                                                          "+
                            " @TABLE_ROW VARCHAR(10),@TASK_ID VARCHAR(30),@TASK_STATUS VARCHAR(5),                                                         "+
                            " @ASSIGNEDBY VARCHAR(50),@UPDATEDBY VARCHAR(50),@TEMP NVARCHAR(30)                                                            "+
                            "                                                                                                                              "+
                            " SET @TABLE_CODE = '"+task.code+"'                                                                                            "+
                            " SET @TABLE_ROW = '"+task.id+"'                                                                                               "+
                            " SET @TASK_ID = '"+task.pk+"'                                                                                                 "+
                            " SET @TASK_STATUS = '"+task.status+"'                                                                                         "+
                            " SET @ASSIGNEDBY = '"+task.assignedby+"'                                                                                      "+
                            " SET @UPDATEDBY = '"+task.updatedby+"'                                                                                        "+
                            "                                                                                                                              "+
                            " SET @TABLE_NAME  = (                                                                                                            " +
                            " 	SELECT TABLE_NAME                                                                                                             " +
                            " 	FROM [CarParkCentral].[dbo].[CP_NOTIFICATION_TABLE]                                                                           " +
                            " 	WHERE TABLE_CODE = @TABLE_CODE                                                                                                " +
                            " )                                                                                                                               " +
                            "                                                                                                                                 " +
                            " SET @QRY = 'SELECT @RESULT = COUNT(*) FROM [CarParkCentral].[dbo].['+@TABLE_NAME+'] WHERE ID='+@TABLE_ROW                       " +
                            " EXECUTE SP_EXECUTESQL @QRY, N'@RESULT VARCHAR(MAX) OUT',  @TEMP OUT                                                             " +
                            " IF(@TEMP > 0)                                                                                                                   " +
                            " BEGIN                                                                                                                           " +
                            " 	SET @QRY = 'SELECT @RESULT = TASK_ID FROM [CarParkCentral].[dbo].['+@TABLE_NAME+'] WHERE ID='+@TABLE_ROW                      " +
                            " 	EXECUTE SP_EXECUTESQL @QRY, N'@RESULT VARCHAR(MAX) OUT',  @TEMP OUT                                                           " +
                            " 	IF(@TEMP IS NULL)                                                                                                             " +
                            " 		BEGIN                                                                                                                     " +
                            " 			SET @QRY = N'UPDATE [CarParkCentral].[dbo].['+@TABLE_NAME+'] SET TASK_ID = '''+@TASK_ID+''' WHERE ID='+@TABLE_ROW     " +
                            " 			EXEC SP_EXECUTESQL @QRY                                                                                               " +
                            " 		END                                                                                                                       " +
                            " 	ELSE                                                                                                                          " +
                            " 		BEGIN                                                                                                                     " +
                            " 			SET @TASK_ID = @TEMP                                                                                                  " +
                            " 		END                                                                                                                       " +
                            " 	BEGIN                                                                                                                         " +
                            " 	   IF NOT EXISTS (                                                                                                            " +
                            " 			SELECT TASK_ID                                                                                                        " +
                            " 			FROM [CarParkCentral].[dbo].[CP_NOTIFICATION_TASK]                                                                    " +
                            " 			WHERE TASK_ID = @TASK_ID                                                                                              " +
                            " 		)                                                                                                                         " +
                            " 			BEGIN                                                                                                                 " +
                            " 				INSERT INTO [CarParkCentral].[dbo].[CP_NOTIFICATION_TASK]                                                         " +
                            " 					([TASK_ID],[TABLE_CODE_FK],[TASK_STATUS],[ASSIGNEDBY],[UPDATEDBY])                                            " +
                            " 				VALUES                                                                                                            " +
                            " 					(@TASK_ID,@TABLE_CODE,@TASK_STATUS,@ASSIGNEDBY,@UPDATEDBY) 		                                              " +
                            " 			END                                                                                                                   " +
                            " 		ELSE                                                                                                                      " +
                            " 			BEGIN                                                                                                                 " +
                            " 				UPDATE [CarParkCentral].[dbo].[CP_NOTIFICATION_TASK]                                                              " +
                            " 				SET [TASK_STATUS] = @TASK_STATUS,                                                                                 " +
                            " 					[UPDATEDBY] = @UPDATEDBY,                                                                                     " +
                            " 					[LASTUPDATE] = GETDATE()                                                                                      " +
                            " 				WHERE TASK_ID = @TASK_ID                                                                                          " +
                            " 			END                                                                                                                   " +
                            " 	END                                                                                                                           " +
                            " END                                                                                                                             ";

            
            sm = new SqlCommand();
            sm.Connection = sqlConnection;
            sm.CommandText = linq;
            sm.CommandType = CommandType.Text;
            openConn();
            SqlDataReader dr = sm.ExecuteReader();
            return dr;

        }
        
        
        public String getGeneratedTime()
        {
            String linq = " SELECT TOP 1 CONVERT(VARCHAR(20),DATETIMECREATED) AS GEN_TIME        " +
                            " FROM [CarParkCentral].[dbo].[CP_CONN_STAT_V]                       " +
                            " WHERE JOB_FROM = 'StatusChecker.ps1'                               " +
                            " ORDER BY DATETIMECREATED DESC                                      ";                                             


            sm = new SqlCommand();
            sm.Connection = sqlConnection;
            sm.CommandText = linq;
            sm.CommandType = CommandType.Text;
            openConn();
            SqlDataReader dr = sm.ExecuteReader();

            String result = "";
            while (dr.Read()) 
            {
               result = dr.GetString(0);
               break;
            }
            closeConn();
            return result;
        }



        public void closeConn()
        {
            if (sqlConnection.State == ConnectionState.Open)
                sqlConnection.Close();    
        }

        public void openConn()
        {
            if (sqlConnection.State == ConnectionState.Closed)
                sqlConnection.Open();
        }


    }
}