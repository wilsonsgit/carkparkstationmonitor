﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarkparkStationMonitor.Models
{
    public class SpaceStatusModel
    {
        public int cpcode { get; set; }
        public int cpipadd { get; set; }
        public int drive { get; set; }
        public int space { get; set; }
    }
}