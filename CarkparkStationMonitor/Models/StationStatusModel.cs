﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarkparkStationMonitor.Models
{
    public class StationStatusModel
    {
        public int cpcode { get; set; }
        public int cpipadd { get; set; }
        public int stationid { get; set; }
        public int stationipadd { get; set; }
        public int status { get; set; }
    }
}